﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

static class TDS {
	public static Vector3 getObjectSize(GameObject obj) {
		return obj.GetComponent<MeshFilter>().mesh.bounds.size;
	}

	public static Vector3 getRealObjectSize(GameObject obj) {
		return new Vector3(obj.GetComponent<MeshFilter>().mesh.bounds.size.x * obj.transform.localScale.x,
		                   obj.GetComponent<MeshFilter>().mesh.bounds.size.y * obj.transform.localScale.y,
		                   obj.GetComponent<MeshFilter>().mesh.bounds.size.z * obj.transform.localScale.z);
	}

	public static bool timerIsOut(ref float elapsed, float interval) {
		if (elapsed >= interval) {
			elapsed = 0;
			return true;
		}
		else {
			elapsed += Time.deltaTime;
			return false;
		}
	}

	public static EnemiesCreateScheduler createEnemiesCreateScheduler(params EnemiesCreateItem[] items) {
		Queue<EnemiesCreateItem> itemsQueue = new Queue<EnemiesCreateItem> ();

		if (null != items) {
			foreach (EnemiesCreateItem each in items) {
				itemsQueue.Enqueue(each);
			}
		}

		return new EnemiesCreateScheduler(itemsQueue);
	}
}
