﻿using UnityEngine;
using System.Collections;

public class TowerPropertiesCalculator {
	private static int defaultRange = 10;
	private static int defaultDamage = 20;
	private static float defaultAttackPeriod = 0.4f;

	private static int[] rangeAddition;
	private static int[] damageAddition;
	private static float[] attackPeriodMultiplier;

	static TowerPropertiesCalculator() {
		rangeAddition = new int[4] { 0, 0, 1, 3 };
		damageAddition = new int[4] { 20, 10, 30, 5 };
		attackPeriodMultiplier = new float[4] { 1.0f, 0.9f, 1.2f, 0.8f };
	}

	public static void calculate(int[] towerTraits, out int range, out int damage, out float attackPeriod) {
		range = defaultRange;
		damage = defaultDamage;
		attackPeriod = defaultAttackPeriod;

		for (int i = 0; i < GM.towerTypes; ++i) {
			range += rangeAddition[i] * towerTraits[i];
			damage += damageAddition[i] * towerTraits[i];

			attackPeriod *= Mathf.Pow(attackPeriodMultiplier[i], towerTraits[i]);
		}
	}
}
