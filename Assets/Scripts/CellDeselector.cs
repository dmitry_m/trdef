﻿using UnityEngine;
using System.Collections;

public class CellDeselector : MonoBehaviour {
	public CellSelector cellSelector;

	public void OnMouseDown () {
		cellSelector.deselect();
	}
}
