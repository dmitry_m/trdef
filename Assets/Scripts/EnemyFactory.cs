﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyFactory : MonoBehaviour {
	public float timerInterval = 0.4f;

	public EnemiesCreateScheduler enemyScheduler { get; set; }	
	public List<Enemy> enemies { get; set; }
	public new bool active { get; set; }

	EnemyFactory() {
		active = false;
		enemies = new List<Enemy>();
	}

	void Start () {	
	}

	void refreshCounter() {
		GM.enemiesCount = enemies.Count;
	}

	void Update () {
		refreshCounter();

		if (active) {
			if (0 == GM.enemiesCount && enemyScheduler.empty()) {
				GM.nextLevel();
			}

			enemyScheduler.execute (this);
		}
	}

	public void createEnemy (GameObject enemyPrefab) {
		GameObject enemyObject = Instantiate (enemyPrefab, transform.position, transform.rotation) as GameObject;
		enemyObject.animation ["walkGoblin"].speed = 4;
		if (!enemyObject) {
			return;
		}

		setPathForEnemy(enemyObject);
		enemyObject.transform.FindChild("Bip001").FindChild("Bip001 Pelvis").gameObject.tag = "Enemy";
		enemyObject.GetComponent<Enemy> ().enemiesFactory = this;

		registerEnemy(enemyObject.GetComponent<Enemy> ());
	}

	void setPathForEnemy(GameObject enemy) {
		if (null == enemy.GetComponent<EnemyMoving>()) {
			enemy.AddComponent ("EnemyMoving");
		}

		enemy.GetComponent<EnemyMoving> ().path = gameObject.GetComponent<EnemyMoving> ().path;
		enemy.GetComponent<EnemyMoving> ().movingEnabled = true;
		enemy.GetComponent<EnemyMoving> ().enemy = enemy.GetComponent<Enemy> ();
	}
	
	public void registerEnemy(Enemy enemy) {
		if (null == enemy) return;

		if (null == enemies.Find(x => x == enemy)) {
			enemies.Add(enemy);
		}
	}
	
	public void unregisterEnemy(Enemy enemy) {
		if (null == enemy) return;

		if (null != enemies.Find(x => x == enemy)) {
			enemies.Remove(enemy);
		}
	}
}
