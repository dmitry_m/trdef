﻿using UnityEngine;
using System.Collections;

/*
 * @remark assumes sphere prefab has "Sphere"
 * 
 */

public class Sphere : MonoBehaviour {
	public float speed = 10;

	private Enemy target = null;
	private int damage = 0;
	private float hitDistance = 0.4f;

	void Update () {
		if (null != target) {
			if (Vector3.Distance(transform.position, target.transform.position) < hitDistance) {
				hit();
			}
			else {
				moveToTarget();
			}
		}
		else {
			Destroy(gameObject);
		}
	}
	
	private void init(Enemy enemy,  int damage) {
		this.target = enemy;
		this.damage = damage;
	}

	public static void launch(GameObject prefab, Enemy target, int damage, Vector3 position, Quaternion rotation) {
		GameObject sphereObject = Instantiate (prefab, position, rotation) as GameObject;
		if (!sphereObject) {
			return;
		}
		
		Sphere sphere = sphereObject.GetComponent<Sphere>();	
		sphere.init(target, damage);
	}

	private void hit() {		
		target.takeDamage(damage);
		Destroy(gameObject);
	}

	private void moveToTarget() {
		transform.position = Vector3.MoveTowards(transform.position, target.transform.position, Time.deltaTime * speed);
	}
}
