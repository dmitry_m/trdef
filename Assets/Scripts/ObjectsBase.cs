﻿using UnityEngine;
using System.Collections;

public class ObjectsBase : MonoBehaviour {
	public GameObject Enemy1 = null;
	public GameObject Enemy2 = null;
	public GameObject Enemy3 = null;
	public GameObject Enemy4 = null;
	
	public GameObject FireSphere = null;
	public GameObject WaterSphere = null;
	public GameObject WindSphere = null;
	public GameObject GroundSphere = null;
	
	public GameObject FireTower = null;
	public GameObject WaterTower = null;
	public GameObject WindTower = null;
	public GameObject GroundTower = null;
}
