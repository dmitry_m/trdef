﻿using UnityEngine;
using System.Collections;

/*
 * @remark create an object after some time interval
 */ 

public class EnemiesCreateItem {
	public int count;
	public float period;
	public GameObject enemyPrefab;

	private float timer = 0;
	
	public EnemiesCreateItem(float period, GameObject enemyPrefab, int count) {
		this.count = count;
		this.period = period;
		this.enemyPrefab = enemyPrefab;
	}

	public EnemiesCreateItem(float period) 
		: this(period, null, 0) {
	}

	private bool create(EnemyFactory enemyFactory) {
		if (count-- <= 0) {
			return true;
		} else {
			enemyFactory.createEnemy (enemyPrefab);
			return false;
		}
	}

	public bool execute(EnemyFactory enemyFactory) {
		if (timer >= period) {
			timer = 0;
			return create(enemyFactory);
		} else {
			timer += Time.deltaTime;
			return false;
		}
	}
}