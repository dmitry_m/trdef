﻿using UnityEngine;
using System.Collections;

public class TowersFactory : MonoBehaviour {
	// public
	public double buttonsWeight = 0.1;
	public double buttonsHeight = 0.08;
	public double buttonsLeft = 0.25;
	public double buttonsTop = 0.95;
	public double buttonsMargin = 0.04;
	public int rows = 10;
	public int cols = 10;
	
	public GameObject buildZone = null;
	public GameObject cellIndicator = null;
		


	// private
	private bool isCellSelected = false;

	private float buildZoneLeft;
	private float buildZoneTop;	
	private float buildZoneWidth;
	private float buildZoneHeight;
	private float buildZoneAlt;

	private float cellWidth;
	private float cellHeight;
	private Vector2 selectedCell;

	private GameObject[,] towers;

	private EnemyFactory enemyFactory;

	void Start () {
		towers = new GameObject[cols, rows];
		
		buildZone.GetComponent<MeshRenderer>().enabled = false;		
		buildZoneAlt = buildZone.transform.position.y;		
		buildZoneWidth = TDS.getRealObjectSize(buildZone).x;
		buildZoneHeight = TDS.getRealObjectSize(buildZone).z;	
		buildZoneLeft = buildZone.transform.position.x - buildZoneWidth / 2;
		buildZoneTop = buildZone.transform.position.z - buildZoneHeight / 2;	
		cellWidth = buildZoneWidth / cols;
		cellHeight = buildZoneHeight / rows;
		cellIndicator.transform.localScale = new Vector3(buildZoneWidth / TDS.getObjectSize(cellIndicator).x / cols,
		                                                 cellIndicator.transform.localScale.y,
		                                                 buildZoneHeight / TDS.getObjectSize(cellIndicator).z / rows);

		cellIndicator.GetComponent<MeshRenderer>().enabled = false;
		
		enemyFactory = GameObject.Find("EnemiesFactory").GetComponent<EnemyFactory>();
	}

	// can be selected from build zone plane
	public void select(Vector3 position) {
		selectedCell = position2Coordinates (position);
		cellIndicator.transform.position = coordinates2Position (selectedCell);

		showBuildZone (true);
		isCellSelected = true;
	}
		
	public void deselect() {
		showBuildZone (false);
		isCellSelected = false;
	}
	
	private void OnGUI() {	
		if (isCellSelected) {
			createTowerOrUpgradeNeeded ();
		}
	}


	// utils

	GameObject createTower (GameObject towerPrefab, GameObject spherePrefab) {
		GameObject tower;

		if (!(tower = Instantiate (towerPrefab, coordinates2Position(selectedCell), buildZone.transform.rotation) as GameObject)) {
			return null;
		}

		tower.transform.position = new Vector3(tower.transform.position.x, 
		                                       buildZoneAlt + TDS.getRealObjectSize(tower).y / 2, 
		                                       tower.transform.position.z);

		tower.GetComponent<Tower>().enemyFactory = enemyFactory;
		tower.GetComponent<Tower>().spherePrefab = spherePrefab;

		return tower;
	}

	void addTower (Tower.TowerType towerType) {
		GameObject towerPrefab;
		GameObject spherePrefab;

		switch (towerType) {
		case Tower.TowerType.Water:
			towerPrefab = GM.store().WaterTower;
			spherePrefab = GM.store().WaterSphere;
			
			break;
		case Tower.TowerType.Fire:
			towerPrefab = GM.store().FireTower;
			spherePrefab = GM.store().FireSphere;
			
			break;
		case Tower.TowerType.Ground:
			towerPrefab = GM.store().GroundTower;
			spherePrefab = GM.store().GroundSphere;
			
			break;
		case Tower.TowerType.Wind:
			towerPrefab = GM.store().WindTower;
			spherePrefab = GM.store().WindSphere;
			
			break;
		default:
			return;
		}

		towers[(int)selectedCell.x, (int)selectedCell.y] = createTower(towerPrefab, spherePrefab);
		towers[(int)selectedCell.x, (int)selectedCell.y].GetComponent<Tower>().towerTraits[(int)towerType]++;
	}

	private void createTowerOrUpgradeNeeded () {
		double buttonOffsetX = (double)(buttonsWeight + buttonsMargin);
		double buttonOffsetY = (double)(0.5 * buttonsHeight);
		
		GameObject towerPrefab;
		GameObject spherePrefab;
		Tower.TowerType towerType;

		if (GUI.Button(getButtonRect(0 * buttonOffsetX, -buttonOffsetY), "1")) {
			towerType = Tower.TowerType.Fire;
		}
		else if (GUI.Button(getButtonRect(1 * buttonOffsetX, -buttonOffsetY), "2")) {
			towerType = Tower.TowerType.Water;
		}
		else if (GUI.Button(getButtonRect(2 * buttonOffsetX, -buttonOffsetY), "3")) {
			towerType = Tower.TowerType.Ground;
		}
		else if (GUI.Button(getButtonRect(3 * buttonOffsetX, -buttonOffsetY), "4")) {
			towerType = Tower.TowerType.Wind;
		}
		else {
			return;
		}

		int level = 1;

		Tower tower = null;
		GameObject towerObject = towers[(int)selectedCell.x, (int)selectedCell.y];
		if (null != towerObject) {
			tower = towerObject.GetComponent<Tower>();
			if (null != tower) {
				level = tower.towerTraits[(int)towerType];
			}
		}

		if (!GM.tryToAddTower(towerType, level)) {
			return;
		}

		if (null != tower) {
			tower.upgrade(towerType);
		}
		else {
			addTower(towerType);
		}
	}
	
	private void showBuildZone (bool show) {
		cellIndicator.GetComponent<MeshRenderer>().enabled = show;
		buildZone.GetComponent<MeshRenderer> ().enabled = show;		
	}

	private Rect getButtonRect(double offsetX, double offsetY) {
		return new Rect ((float)(Screen.width * (buttonsLeft + offsetX)), 
		                 (float)(Screen.height * (buttonsTop + offsetY)), 
		                 (float)(Screen.width * buttonsWeight), 
		                 (float)(Screen.height * buttonsHeight));
	}

	private Vector3 getHitPoint() {
		Ray mouseRay = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);

		RaycastHit mouseRayHit;
		if (Physics.Raycast (mouseRay, out mouseRayHit, 100000.0f)) {
			return mouseRayHit.point;
		}

		return new Vector3(); 
	}
	
	
	private Vector3 adjustPosition(Vector3 position) {				
		Vector3 adjustedPosition = position;
		adjustedPosition.y = buildZone.transform.position.y + 0.1f;
		
		if (adjustedPosition.x < buildZoneLeft) {
			adjustedPosition.x = buildZoneLeft;
		}
		
		if (adjustedPosition.x >= buildZoneLeft + buildZoneWidth) {
			adjustedPosition.x = buildZoneLeft + buildZoneWidth - 1;
		}
		
		if (adjustedPosition.z < buildZoneTop) {
			adjustedPosition.z = buildZoneTop;
		}

		if (adjustedPosition.z >= buildZoneTop + buildZoneHeight) {
			adjustedPosition.z = buildZoneTop + buildZoneHeight - 1;
		}

		return adjustedPosition;
	}
	
	private bool isInBuildArea(Vector3 position) {				
		return (adjustPosition (position) == position);
	}
	
	private Vector2 position2Coordinates(Vector3 position) {
		return new Vector2((int)((position.x - buildZoneLeft) / cellWidth), 
		                   (int)((position.z - buildZoneTop) / cellHeight));
	}
	
	private Vector3 coordinates2Position(Vector2 coordinates) {
		return new Vector3 (buildZoneLeft + coordinates.x * cellWidth + cellWidth / 2,
		                    buildZoneAlt,
		                    buildZoneTop + coordinates.y * cellHeight + cellHeight / 2);
	}


}
