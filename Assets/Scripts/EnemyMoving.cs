﻿using UnityEngine;
using System.Collections;

public class EnemyMoving : MonoBehaviour {
	public bool movingEnabled = false;
	public Transform[] path;
	
	public bool finishedMoving { get; set; }
	public Enemy enemy { get; set; }

	private NavMeshAgent navMeshAgent;
	private int currentWaypointIndex = 0;

	void Start () {
		finishedMoving = false;
		currentWaypointIndex = 0;
		navMeshAgent = gameObject.GetComponent<NavMeshAgent>();	
		navMeshAgent.enabled = movingEnabled;

		UpdateWaypoint ();	
	}

	void Update () {
		if (finishedMoving || !ValidPath ()) {
			return;
		}

		if (Vector3.Distance (transform.position, path [currentWaypointIndex].position) < 2.0f) {
			currentWaypointIndex++;
			UpdateWaypoint();
		}		
	}

	bool ValidPath() {
		return (path != null 
	       		&& currentWaypointIndex < path.Length);
	}
	
	void HandleFinishMoving () {
		navMeshAgent.enabled = false;

		if (null != enemy) {
			enemy.successfullyFinished();
		}
	}
	
	void UpdateWaypoint () {
		if (ValidPath ()) {			
			if (navMeshAgent.enabled) {
				navMeshAgent.SetDestination (path [currentWaypointIndex].position);
			}
		}
		else {
			HandleFinishMoving ();
		}

	}
}
