﻿using UnityEngine;
using System.Collections;


public class LevelSetup : MonoBehaviour {
	public string labelCaption { get; set; }
	private Color labelColor = Color.white;
	private float timer = 0;
	private bool active = true;
	private bool firstTime = true;

	void Launch() {
		GM.Launch();
	}

	void Start() {
		labelCaption = "GO";

		GM.Init();
	}

	public void reset(string newCaption) {
		timer = 0;
		labelCaption = newCaption;
		labelColor = Color.white;
		active = true;
	}

	void Update () {
		if (!active) {
			return;
		}

		const float delay = 1.0f;
		const float fadingOutTime = 1.0f;
		
		if (timer > delay) {
			labelColor.a -= fadingOutTime * Time.deltaTime;
		}
		if (timer > delay + fadingOutTime) {
			if (firstTime) Launch();
			active = false;
		}
		timer += Time.deltaTime;
	}
	
	void OnGUI() {
		GUI.color = labelColor;
		GUI.Label (new Rect(Screen.width / 2, Screen.height / 2, 100,50), labelCaption);
	}
}
