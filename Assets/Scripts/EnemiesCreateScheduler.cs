﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemiesCreateScheduler {
	private Queue<EnemiesCreateItem> enemiesCreateItems = null;
	private EnemiesCreateItem currentItem = null;

	public EnemiesCreateScheduler (Queue<EnemiesCreateItem> items)
	{
		enemiesCreateItems = items;
	}

	/*
	 * @remark returns true if work is done
	 */ 
	public bool execute(EnemyFactory enemyFactory) {
		GM.enemiesCount = enemiesCreateItems.Count + (null != currentItem ? 1 : 0);

		if ((null == currentItem)
		    && (null != enemiesCreateItems && 0 < enemiesCreateItems.Count)) {
				currentItem = enemiesCreateItems.Dequeue ();
		}

		if (null != currentItem) {
			if (currentItem.execute (enemyFactory)) {
				currentItem = null;
			}
		}
			
		return (null == currentItem);
	}

	public bool empty() {
		return (null == currentItem 
		        && (null != enemiesCreateItems && 0 == enemiesCreateItems.Count));
	}
}
