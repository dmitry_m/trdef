﻿using UnityEngine;
using System.Collections;

public static class GM {
	public static int towerTypes = 4;
	public static Rect livesLabel;
	public static Rect moneyLabel;
	
	public static int level { get; set; }
	public static int enemiesCount { get; set; }
	private const int maxLevels = 6;

	private static int lives { get; set; }
	private static int money { get; set; }
	private static int[] towerCost = new int[] { 10, 10, 10, 10 };
	private static bool launched { get; set; }
	private static EnemyFactory enemyFactory { get; set; }

	static GM() {
		lives = 20;
		level = 1;
		money = 100;
		launched = false;
	}
	
	public static void Init() {
		if ("GameLevel1" != Application.loadedLevelName) {
			return;
		}
		
		enemyFactory = GameObject.Find("EnemiesFactory").GetComponent<EnemyFactory>();
		updateIndicator();
	}

	static void updateIndicator() {
		GameObject.Find("Main Camera").GetComponent<Indicators> ().indicatorCaption = lives;
	}

	public static void enemyPassed() {		
		if (0 == --lives) {
			Application.LoadLevel("Menu");
		}
		updateIndicator();
	}

	public static void startLevel() {
		spawnEnemies();
		LevelSetup levelSetup = GameObject.Find("Main Camera").GetComponent<LevelSetup>();		
		levelSetup.reset("LEVEL " + GM.level);
		updateIndicator();
	}

	public static void spawnEnemies() {		
		enemyFactory.enemyScheduler = TDS.createEnemiesCreateScheduler (getEnemiesCreateItems());
	}

	public static void nextLevel() {		
		if (++level < maxLevels) {
			startLevel();
		}
		else {
			Application.LoadLevel("Menu");
		}
	}

	public static void Launch () {
		if (launched || "GameLevel1" != Application.loadedLevelName) {
			return;
		}

		enemyFactory.active = true;	

		startLevel();

		launched = true;
	}

	public static bool tryToAddTower(Tower.TowerType towerType, int level) {
		if (money >= towerCost[(int)towerType]) {
			money -= towerCost[(int)towerType];
			return true;
		}
		return false;
	}

	public static ObjectsBase store() {
		return GameObject.Find("ObjectsBase").GetComponent<ObjectsBase>();
	}
	
	private static EnemiesCreateItem[] getEnemiesCreateItems() {				
		ObjectsBase objectsBase = GameObject.Find("ObjectsBase").GetComponent<ObjectsBase>();
		
		switch (level) {
		case 1:
			return new EnemiesCreateItem[] {
				new EnemiesCreateItem(0.5f, objectsBase.Enemy1, 10)
			};
		case 2:
			return new EnemiesCreateItem[] {
				new EnemiesCreateItem(0.5f, objectsBase.Enemy1, 20)
			};
		case 3:
			return new EnemiesCreateItem[] {
				new EnemiesCreateItem(0.3f, objectsBase.Enemy1, 30)
			};
		case 4:
			return new EnemiesCreateItem[] {
				new EnemiesCreateItem(0.3f, objectsBase.Enemy1, 30),
				new EnemiesCreateItem(1.0f),
				new EnemiesCreateItem(0.3f, objectsBase.Enemy1, 30),
			};
		case 5:
			return new EnemiesCreateItem[] {
				new EnemiesCreateItem(0.2f, objectsBase.Enemy1, 20),
				new EnemiesCreateItem(1.0f),
				new EnemiesCreateItem(0.2f, objectsBase.Enemy1, 20),
				new EnemiesCreateItem(1.0f),
				new EnemiesCreateItem(0.2f, objectsBase.Enemy1, 20)
			};
		case 6:
			return new EnemiesCreateItem[] {
				new EnemiesCreateItem(0.12f, objectsBase.Enemy1, 20),
				new EnemiesCreateItem(1.0f),
				new EnemiesCreateItem(0.12f, objectsBase.Enemy1, 20),
				new EnemiesCreateItem(1.0f),
				new EnemiesCreateItem(0.12f, objectsBase.Enemy1, 20)
			};
			
		default:
			return null;
		}
	}
}
