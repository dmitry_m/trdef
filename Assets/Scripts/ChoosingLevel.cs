﻿using UnityEngine;
using System.Collections;

public class ChoosingLevel : MonoBehaviour {
	public int buttonWidth = 50;
	public int buttonHeight = 40;
	public int margin = 20;
	public float menuLeft = 0.5f;
	public float menuTop = 0.5f;
	public int cols = 3;
	public int rows = 2;
	public float mainMenuButtonLeft = 0.5f;
	public float mainMenuButtonTop = 0.5f;
	public int mainMenuButtonWidth = 100;
	public int mainMenuButtonHeight = 50;

	void OnGUI () {
		int width = (cols * buttonWidth + (cols - 1) * margin);
		int height = (rows * buttonHeight + (rows - 1) * margin);

		int left = (int)(Screen.width * menuLeft - width/2);
		int top = (int)(Screen.height * menuTop - height/2);

		for (int row = 0; row < rows; ++row) {			
			for (int col = 0; col < cols; ++col) {				

				int level = row * cols + col + 1;
				string caption = (string)level.ToString().Clone();
				if (GUI.Button(new Rect(left + col * (buttonWidth + margin), 
					            		top + row * (buttonHeight + margin), 
					                    buttonWidth, 
					                    buttonHeight), 
				               level.ToString()))
				{
					GM.level = level;
					Application.LoadLevel("GameLevel1");
				}
			}
		}
		
		if (GUI.Button(new Rect((int)(Screen.width * mainMenuButtonLeft - mainMenuButtonWidth/2), 
		                        (int)(Screen.height * mainMenuButtonTop - mainMenuButtonHeight/2), 
		                        mainMenuButtonWidth, 
		                        mainMenuButtonHeight), 
		               "Back"))
		{
			Application.LoadLevel("MainMenu");
		}
	}
}
