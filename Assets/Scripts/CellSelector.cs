﻿using UnityEngine;
using System.Collections;

public class CellSelector : MonoBehaviour {

	public TowersFactory towerFactory;	
	public double delay = 0.6f;

	public bool selectMode { get; set; }

	private double timer;

	// Use this for initialization
	void Start () {
		selectMode = false;
		timer = 0;
	}

	private Vector3 getHitPoint() {
		Ray mouseRay = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);
		
		RaycastHit mouseRayHit;
		if (Physics.Raycast (mouseRay, out mouseRayHit, 100000.0f)) {
			return mouseRayHit.point;
		}
		
		return new Vector3(); 
	}
	
	public void select() {
		towerFactory.select (getHitPoint());

		selectMode = true;
	}
	
	public void deselect() { 
		towerFactory.deselect ();

		selectMode = false;		
	}


	public void OnMouseDrag () {
		if (selectMode) {
			select ();
		}
		else if (timer > delay) {
			select ();
			timer = 0;
		}
		else {
			timer += Time.deltaTime;	
		}
	}

	public void OnMouseUp () {
		timer = 0;
	}
}
