﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {		
	public double healthBarWidth = 0.02;
	public double healthBarHeight = 0.002;
	public double altOffset = 0.02;

	private double healthProportion = 1;
	public double HealthProportion { 
		get {
			return healthProportion;
		}
		set {
			healthProportion = (value < 0 || value > 1 ? 1 : value);
		}
	}

	private float alt;
	private float width;
	private float height;
		
	void Start () {	
		healthProportion = 1;

		alt = (float)(altOffset * Screen.width);
		width = (float)(Screen.width * healthBarWidth);
		height = (float)(Screen.height * healthBarHeight);
	}	
	
	void Update () {		
		//AddjustCurrentHealth(0);  		
	}	
		
	void OnGUI() {
		Vector3 healthPosition = Camera.main.WorldToScreenPoint(transform.position);
		healthPosition.y += (float)alt;

		float green = (float)HealthProportion;
		float red = (green > 0 ? 0.9f - green : 0.9f);
		Color color = new Color(red, green, 0);

		Texture2D texture = new Texture2D(1, 1);
		texture.SetPixel(0,0,color);
		texture.Apply();
		
		//GUIContent guiContent = new GUIContent(curHealth + "/" + maxHealth);
		GUI.skin.box.normal.background = texture;
		GUI.Box(new Rect(healthPosition.x - width/2, Screen.height - healthPosition.y - height/2, width, height), GUIContent.none);
	}
		/*
	public void AddjustCurrentHealth(int adj) {		
		curHealth += adj;
				
		if (curHealth < 0)			
			curHealth = 0;		
		
		if (curHealth > maxHealth)			
			curHealth = maxHealth;
				
		if(maxHealth < 1)			
			maxHealth = 1;		
				
		healthBarLength = Screen.width * healthBarLength * (curHealth /(float)maxHealth);
	}	*/
}