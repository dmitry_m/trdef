﻿using UnityEngine;
using System.Collections;

public class Indicators : MonoBehaviour {
	public float left = 0.9f;
	public float top = 0.9f;

	public int indicatorCaption { get; set; }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
		
	void OnGUI() {
		GUI.Label (new Rect(Screen.width * left, Screen.height * top, 200,50), indicatorCaption.ToString());
	}
}
