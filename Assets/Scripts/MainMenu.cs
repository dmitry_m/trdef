﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	public int buttonWidth = 200;
	public int buttonHeight = 40;
	public int margin = 20;
	public float menuLeft = 0.5f;
	public float menuTop = 0.5f;

	void OnGUI() 
	{
		int left = (int)(Screen.width * menuLeft - buttonWidth/2);
		int top = (int)(Screen.height * menuTop - buttonHeight/2);
		
		if (GUI.Button(new Rect(left, top, buttonWidth, buttonHeight), "Start"))
		{
			Application.LoadLevel("ChoosingLevel");
		}
		if (GUI.Button(new Rect(left, top + margin + buttonHeight * 1, buttonWidth, buttonHeight), "Exit"))
		{
			Application.Quit();
		}
	}
}
