﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {
	public EnemyFactory enemiesFactory { get; set; }
	
	public int maxHealth = 100;	
	public int curHealth = 100;	

	void Start () {
	}

	void Update () {
	
	}

	public void takeDamage(int value) {
		curHealth -= value;

		GetComponent<EnemyHealth>().HealthProportion = ((double)curHealth / maxHealth);

		if (curHealth <= 0) {
			die();
		}
	}

	public void die() {
		removeEnemy();
	}

	public void successfullyFinished() {
		removeEnemy();
		GM.enemyPassed();
	}

	private void removeEnemy() {
		enemiesFactory.unregisterEnemy(this);
		Destroy(gameObject);
	}
}
