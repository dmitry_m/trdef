﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tower : MonoBehaviour {	
	public enum TowerType {
		Fire = 0,
		Water = 1,
		Ground = 2,
		Wind = 3
	}	 
	
	public GameObject spherePrefab { get; set; }
	public EnemyFactory enemyFactory { get; set; }
	public int[] towerTraits { get; set; }
	
	private int range = 15;
	private int damage = 40;
	private float attackPeriod = 0.40f;

	private GameObject[] towerIndicators;
	private Enemy currentTarget;
	private float attackTimer = 0;

	Tower() {
		initTraits();
	}

	void Start () {	
		initIndicators();
		
		
		refreshTraits();
	}

	void initTraits() {
		towerTraits = new int[GM.towerTypes];
		for (int i = 0; i < GM.towerTypes; ++i) {
			towerTraits[i] = 0;
		}	
	}
	
	void initIndicators() {
		int margin = 10;
		
		towerIndicators = new GameObject[GM.towerTypes];

		GameObject go = createIndicator(GM.store().FireSphere, transform.position.y + margin * 1);		
		towerIndicators[(int)TowerType.Fire] = go;

		go = createIndicator(GM.store().WaterSphere, transform.position.y + margin * 2);	
		towerIndicators[(int)TowerType.Water] = go;

		go = createIndicator(GM.store().GroundSphere, transform.position.y + margin * 3);	
		towerIndicators[(int)TowerType.Ground] = go;

		go = createIndicator(GM.store().WindSphere, transform.position.y + margin * 1);	
		towerIndicators[(int)TowerType.Wind] = go;
	}

	GameObject createIndicator(GameObject prefab, float alt) {
		Vector3 position = transform.position;
		position.y += alt;

		GameObject indicator = Instantiate (prefab, position, transform.rotation) as GameObject;
		if (!indicator) {
			return null;
		}

		indicator.GetComponent<MeshRenderer>().enabled = false;

		return indicator;
	}

	void Update () {
		lookForTarget();

		if (TDS.timerIsOut(ref attackTimer, attackPeriod)) {
			attackTarget();
		}
	}

	public void refreshTraits() {	
		for (int i = 0; i < 4; ++i) {
			float scale = 1.0f + 0.2f * towerTraits[i];

			towerIndicators[i].GetComponent<MeshRenderer>().enabled = (towerTraits[i] > 0);
			towerIndicators[i].GetComponent<Transform>().localScale = new Vector3(scale, scale, scale);
		}
		TowerPropertiesCalculator.calculate(towerTraits, out range, out damage, out attackPeriod);
	}

	public void upgrade(Tower.TowerType towerType) {
		towerTraits[(int)towerType]++;

		refreshTraits();
	}

	public void resetTarget() {
		currentTarget = null;
	}

	private void searchForTarget() {
		float minimalDistance = 1000;
		Enemy closestEnemy = null;

		foreach (Enemy enemy in enemyFactory.enemies) {
			float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);

			if (minimalDistance > distanceToEnemy) {
				minimalDistance = distanceToEnemy;
				closestEnemy = enemy;
			}
		}

		if (minimalDistance < range) {
			currentTarget = closestEnemy;
		}
	}

	private void checkTarget() {
		if (Vector3.Distance(transform.position, currentTarget.transform.position) > range) {
			currentTarget = null;
		}
	}

	private void lookForTarget() {
		if (null == currentTarget) {	
			searchForTarget();
		}
		else {
			checkTarget();
		}
	}

	private void launchSphere () {

		Sphere.launch(spherePrefab, currentTarget, damage, transform.position, transform.rotation);
	}
	
	private void attackTarget() {
		if (null == currentTarget) {
			return;
		}

		//gameObject.transform.LookAt(currentTarget.transform.position);

		launchSphere ();	
	}
}
