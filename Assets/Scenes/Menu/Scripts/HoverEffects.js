﻿#pragma strict

// each button needs to init
var newGameBtn = false;
var exitBtn = false;
var optionsBtn = false;
var bestQualityBtn = false;
var worstQualityBtn = false;
var cancelFromOptionsBtn = false;
// lightes for each button
var newGameLight : Light;
var exitLight : Light;
var optionsLight : Light;
var bestQualityLight : Light;
var worstQualityLight : Light;
// main and options cameras
var mainCamera : Camera;
var optionsCamera : Camera;

function OnMouseEnter() {
	var k = 1;
	if(exitBtn) {
		while(k!=6) {
			yield WaitForSeconds(0.03); 
			exitLight.intensity = k; 
			k+=1; 
		}
	} else 
	if(newGameBtn) {
		while(k!=6) {
			yield WaitForSeconds(0.03); 
			newGameLight.intensity = k; 
			k+=1; 
		}
	} else 
	if(optionsBtn) {
		while(k!=6) {
			yield WaitForSeconds(0.03); 
			optionsLight.intensity = k; 
			k+=1; 
		}
	} else 
	if(bestQualityBtn) {
		while(k!=6) {
			yield WaitForSeconds(0.03); 
			bestQualityLight.intensity = k; 
			k+=1; 
		}
	} else 
	if(worstQualityBtn) {
		while(k!=6) {
			yield WaitForSeconds(0.03); 
			worstQualityLight.intensity = k; 
			k+=1; 
		}
	}
}
function OnMouseExit() {
	var k = 6;
	if(exitBtn) {
		while(k!=1) {
			yield WaitForSeconds(0.03); 
			exitLight.intensity = k; 
			k-=1; 
		}
	} else
	if(newGameBtn) {
		while(k!=1) {
			yield WaitForSeconds(0.03); 
			newGameLight.intensity = k; 
			k-=1; 
		}
	} else
	if(optionsBtn) {
		while(k!=1) {
			yield WaitForSeconds(0.03); 
			optionsLight.intensity = k; 
			k-=1; 
		}
	} else
	if(worstQualityBtn) {
		while(k!=1) {
			yield WaitForSeconds(0.03); 
			worstQualityLight.intensity = k; 
			k-=1; 
		}
	} else
	if(bestQualityBtn) {
		while(k!=1) {
			yield WaitForSeconds(0.03); 
			bestQualityLight.intensity = k; 
			k-=1; 
		}
	}
}
function OnMouseUp() {
	if(exitBtn) { 
		Application.Quit(); 
	} else
	if(newGameBtn) {
		Application.LoadLevel("GameLevel1");
	} else
	if(optionsBtn) {
		mainCamera.active = false;
		optionsCamera.active = true;
	} else
	if(bestQualityBtn) {
		QualitySettings.currentLevel = QualityLevel.Fantastic;
	} else
	if(worstQualityBtn) {
		QualitySettings.currentLevel = QualityLevel.Fastest;
	} else 
	if(cancelFromOptionsBtn) {
		optionsCamera.active = false;
		mainCamera.active = true;
	}
}